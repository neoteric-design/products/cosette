# Cosette

A simplified test theme built by Neoteric Design for Hugo.

# Setup and install

1. Create a hugo site: `hugo new site <yoursitename>`
1. Add to this theme as a module to your config.toml: 
```
[[module.imports]]
path = "gitlab.com/neoteric-design/products/cosette"
```
3. Replace your `/archetype/default.md` with cosette's /archetype/default.md
4. Create a page: `hugo new <pagename>.md` should generate a file in \content
5. Run the hugo server and check it out: `hugo server`
